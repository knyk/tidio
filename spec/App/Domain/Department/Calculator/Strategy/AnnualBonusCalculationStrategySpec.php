<?php

declare(strict_types=1);

namespace spec\App\Domain\Department\Calculator\Strategy;

use App\Domain\Department\Bonus;
use App\Domain\Department\Department;
use App\Domain\Employee\Employee;
use App\Domain\Shared\Provider\DateTimeProvider;
use PhpSpec\ObjectBehavior;

class AnnualBonusCalculationStrategySpec extends ObjectBehavior
{
    public function let(DateTimeProvider $dateTimeProvider): void
    {
        $this->beConstructedWith($dateTimeProvider);
    }

    public function it_should_return_bonus_for_10_years_if_experience_if_greater(
        Employee $employee,
        DateTimeProvider $dateTimeProvider,
        Department $department,
        Bonus $bonus
    ): void {
        $employee->employedSince()->willReturn(new \DateTimeImmutable('2009-01-01'));

        $dateTimeProvider->now()->willReturn(new \DateTimeImmutable('2021-01-01'));

        $employee->department()->willReturn($department);

        $department->bonus()->willReturn($bonus);

        $bonus->value()->willReturn(10);

        $this->calculate($employee)->shouldBe(100.);
    }

    public function it_should_return_bonus_for_10_years_if_experience_equal_to_10_yeras(
        Employee $employee,
        DateTimeProvider $dateTimeProvider,
        Department $department,
        Bonus $bonus
    ): void {
        $employee->employedSince()->willReturn(new \DateTimeImmutable('2011-01-01'));

        $dateTimeProvider->now()->willReturn(new \DateTimeImmutable('2021-01-01'));

        $employee->department()->willReturn($department);

        $department->bonus()->willReturn($bonus);

        $bonus->value()->willReturn(10);

        $this->calculate($employee)->shouldBe(100.);
    }

    public function it_should_return_bonus_for_experience_years(
        Employee $employee,
        DateTimeProvider $dateTimeProvider,
        Department $department,
        Bonus $bonus
    ): void {
        $employee->employedSince()->willReturn(new \DateTimeImmutable('2019-01-01'));

        $dateTimeProvider->now()->willReturn(new \DateTimeImmutable('2021-01-01'));

        $employee->department()->willReturn($department);

        $department->bonus()->willReturn($bonus);

        $bonus->value()->willReturn(10);

        $this->calculate($employee)->shouldBe(20.);
    }
}
