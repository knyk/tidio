<?php

declare(strict_types=1);

namespace spec\App\Domain\Department\Calculator\Strategy;

use App\Domain\Department\Bonus;
use App\Domain\Department\Department;
use App\Domain\Employee\Employee;
use PhpSpec\ObjectBehavior;

class PercentageBonusCalculationStrategySpec extends ObjectBehavior
{
    public function it_should_return_calculated_bonus(Employee $employee, Department $department, Bonus $bonus): void
    {
        $employee->salary()->willReturn(120);

        $employee->department()->willReturn($department);

        $department->bonus()->willReturn($bonus);

        $bonus->value()->willReturn(15);

        $this->calculate($employee)->shouldBe(18.);
    }
}
