<?php

declare(strict_types=1);

namespace spec\App\Domain\Department\Calculator;

use App\Domain\Department\Bonus;
use App\Domain\Department\BonusType;
use App\Domain\Department\Calculator\Exception\NotSatisfiedCalculationStrategy;
use App\Domain\Department\Calculator\Strategy\BonusCalculationStrategy;
use App\Domain\Department\Department;
use App\Domain\Employee\Employee;
use PhpSpec\ObjectBehavior;

class BonusCalculatorSpec extends ObjectBehavior
{
    public function let(BonusCalculationStrategy $bonusCalculationStrategy): void
    {
        $this->beConstructedWith($bonusCalculationStrategy);
    }

    public function it_should_throw_exception_if_non_strategy_is_satisfied(
        BonusCalculationStrategy $bonusCalculationStrategy,
        Employee $employee,
        Department $department,
        Bonus $bonus
    ): void {
        $bonusType = BonusType::percentage();

        $bonusCalculationStrategy->isSatisfiedBy($bonusType)->shouldBeCalledOnce()->willReturn(false);

        $employee->department()->willReturn($department);

        $department->bonus()->willReturn($bonus);

        $bonus->type()->willReturn($bonusType);

        $this->shouldThrow(NotSatisfiedCalculationStrategy::withBonusType($bonusType->type()))->during(
            'calculate',
            [$employee]
        );
    }

    public function it_should_return_calculated_bonus_from_satisfied_stategy(
        BonusCalculationStrategy $bonusCalculationStrategy,
        Employee $employee,
        Department $department,
        Bonus $bonus
    ): void {
        $bonusType = BonusType::percentage();

        $calculatedBonus = 10.5;

        $bonusCalculationStrategy->isSatisfiedBy($bonusType)->shouldBeCalledOnce()->willReturn(true);
        $bonusCalculationStrategy->calculate($employee)->shouldBeCalledOnce()->willReturn($calculatedBonus);

        $employee->department()->willReturn($department);

        $department->bonus()->willReturn($bonus);

        $bonus->type()->willReturn($bonusType);

        $this->calculate($employee)->shouldBe($calculatedBonus);
    }
}
