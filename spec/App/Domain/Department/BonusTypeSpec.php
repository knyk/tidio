<?php

declare(strict_types=1);

namespace spec\App\Domain\Department;

use App\Domain\Department\Exception\InvalidBonusType;
use PhpSpec\ObjectBehavior;

class BonusTypeSpec extends ObjectBehavior
{
    public function it_should_throw_exception_if_provided_type_is_invalid(): void
    {
        $this->beConstructedWith('dummy');

        $this->shouldThrow(InvalidBonusType::withType('dummy'))->duringInstantiation();
    }
}
