<?php

declare(strict_types=1);

namespace spec\App\Application\Handler;

use App\Application\Query\Model\Factory\SalaryReportResultFactory;
use App\Application\Query\Model\SalaryReportResult;
use App\Application\Query\Model\SalaryReportRow;
use App\Application\Query\SalaryReport;
use App\Application\Query\Sort\SalaryReportResultSorter;
use App\Application\Query\Sort\SalaryReportSort;
use App\Domain\Employee\Criteria\ListCriteria;
use App\Domain\Employee\Employee;
use App\Domain\Employee\EmployeesRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SalaryReportHandlerSpec extends ObjectBehavior
{
    public function let(
        EmployeesRepository $employeesRepository,
        SalaryReportResultFactory $salaryReportResultFactory,
        SalaryReportResultSorter $salaryReportResultSorter
    ): void {
        $this->beConstructedWith($employeesRepository, $salaryReportResultFactory, $salaryReportResultSorter);
    }

    public function it_should_return_unsorted_results_if_sort_field_is_empty(
        EmployeesRepository $employeesRepository,
        Employee $employee,
        SalaryReportResultFactory $salaryReportResultFactory,
        SalaryReportResultSorter $salaryReportResultSorter
    ): void {
        $listCriteria = new ListCriteria('firstname', 'surname', 'department');

        $employeesRepository->list($listCriteria)->shouldBeCalledOnce()->willReturn([$employee]);

        $salaryReportResult = new SalaryReportResult();

        $salaryReportResultFactory->create($employee)->shouldBeCalledOnce()->willReturn($salaryReportResult);

        $salaryReportResultSorter->sort(Argument::cetera())->shouldNotBeCalled();

        $salaryReport = new SalaryReport($listCriteria, new SalaryReportSort());

        $this->__invoke($salaryReport)->shouldBe($salaryReportResult);
    }

    public function it_should_return_sorted_results_if_sort_field_is_set(
        EmployeesRepository $employeesRepository,
        Employee $employee,
        SalaryReportResultFactory $salaryReportResultFactory,
        SalaryReportResultSorter $salaryReportResultSorter
    ): void {
        $listCriteria = new ListCriteria('firstname', 'surname', 'department');

        $employeesRepository->list($listCriteria)->shouldBeCalledOnce()->willReturn([$employee]);

        $salaryReportResult = new SalaryReportResult();
        $items = [new SalaryReportRow()];
        $salaryReportResult->items = $items;

        $salaryReportResultFactory->create($employee)->shouldBeCalledOnce()->willReturn($salaryReportResult);

        $sortedItems = [new SalaryReportRow()];
        $salaryReportResultSorter->sort('firstname', true, $salaryReportResult->items)
            ->shouldBeCalledOnce()->willReturn($items);

        $salaryReport = new SalaryReport($listCriteria, new SalaryReportSort('firstname', 'asc'));

        $this->__invoke($salaryReport)->shouldBe($salaryReportResult);
    }
}
