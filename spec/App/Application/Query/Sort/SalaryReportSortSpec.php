<?php

declare(strict_types=1);

namespace spec\App\Application\Query\Sort;

use App\Application\Query\Sort\Exception\InvalidSortDirection;
use App\Application\Query\Sort\Exception\InvalidSortField;
use PhpSpec\ObjectBehavior;

class SalaryReportSortSpec extends ObjectBehavior
{
    public function it_should_throw_exception_if_field_is_invalid(): void
    {
        $this->beConstructedWith('dummyfield', 'asc');

        $this->shouldThrow(InvalidSortField::withField('dummyfield'))->duringInstantiation();
    }

    public function it_should_throw_exception_if_direction_is_invalid(): void
    {
        $this->beConstructedWith('firstname', 'dummy');

        $this->shouldThrow(InvalidSortDirection::withDirection('dummy'))->duringInstantiation();
    }

    public function it_should_return_true_if_direction_is_ascending(): void
    {
        $this->beConstructedWith('firstname', 'asc');

        $this->isAscending()->shouldBe(true);
    }

    public function it_should_return_false_if_direction_is_descending(): void
    {
        $this->beConstructedWith('firstname', 'desc');

        $this->isAscending()->shouldBe(false);
    }
}
