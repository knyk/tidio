<?php

declare(strict_types=1);

namespace spec\App\Application\Query\Sort;

use App\Application\Query\Model\SalaryReportRow;
use PhpSpec\ObjectBehavior;

class SalaryReportResultSorterSpec extends ObjectBehavior
{
    public function it_should_return_ascending_sorted_array(): void
    {
        $salaryReportRow1 = new SalaryReportRow();
        $salaryReportRow1->firstname = 'a';

        $salaryReportRow2 = new SalaryReportRow();
        $salaryReportRow2->firstname = 'b';

        $unsortedArray = [$salaryReportRow2, $salaryReportRow1];

        $this->sort('firstname', true, $unsortedArray)->shouldBe([$salaryReportRow1, $salaryReportRow2]);
    }

    public function it_should_return_descending_sorted_array(): void
    {
        $salaryReportRow1 = new SalaryReportRow();
        $salaryReportRow1->firstname = 'a';

        $salaryReportRow2 = new SalaryReportRow();
        $salaryReportRow2->firstname = 'b';

        $unsortedArray = [$salaryReportRow1, $salaryReportRow2];

        $this->sort('firstname', false, $unsortedArray)->shouldBe([$salaryReportRow2, $salaryReportRow1]);
    }
}
