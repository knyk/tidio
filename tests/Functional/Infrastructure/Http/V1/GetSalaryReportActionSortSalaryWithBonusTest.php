<?php

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Http\V1;

use App\Infrastructure\Fixtures\Doctrine\DepartmentsFixtures;
use App\Infrastructure\Fixtures\Doctrine\EmployeesFixtures;
use App\Tests\DoctrineFixturesTrait;
use Coduo\PHPMatcher\PHPUnit\PHPMatcherAssertions;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetSalaryReportActionSortSalaryWithBonusTest extends WebTestCase
{
    use DoctrineFixturesTrait;
    use PHPMatcherAssertions;

    public function testResponseShouldShouldBeSortedAscBySalaryWithBonus(): void
    {
        $client = self::createClient();

        $this->loadDoctrineFixtures($client->getContainer(), new DepartmentsFixtures(), new EmployeesFixtures());

        $client->request(
            'GET',
            '/api/v1/salary-reports',
            ['sortField' => 'salaryWithBonus', 'sortDirection' => 'asc']
        );

        self::assertJsonStringEqualsJsonString(
            '{
                "items": [
                    {
                        "firstname": "Ania",
                        "surname": "Nowak",
                        "department": "BOK",
                        "basicSalary": 1100,
                        "bonus": 110,
                        "bonusType": "percentage",
                        "salaryWithBonus": 1210
                    },
                    {
                        "firstname": "Adam",
                        "surname": "Kowalski",
                        "department": "HR",
                        "basicSalary": 1000,
                        "bonus": 1000,
                        "bonusType": "annual",
                        "salaryWithBonus": 2000
                    }
                ]
            }',
            $client->getResponse()->getContent()
        );
    }

    public function testResponseShouldShouldBeSortedDescBySalaryWithBonus(): void
    {
        $client = self::createClient();

        $this->loadDoctrineFixtures($client->getContainer(), new DepartmentsFixtures(), new EmployeesFixtures());

        $client->request(
            'GET',
            '/api/v1/salary-reports',
            ['sortField' => 'salaryWithBonus', 'sortDirection' => 'desc']
        );

        self::assertJsonStringEqualsJsonString(
            '{
                "items": [
                    {
                        "firstname": "Adam",
                        "surname": "Kowalski",
                        "department": "HR",
                        "basicSalary": 1000,
                        "bonus": 1000,
                        "bonusType": "annual",
                        "salaryWithBonus": 2000
                    },
                    {
                        "firstname": "Ania",
                        "surname": "Nowak",
                        "department": "BOK",
                        "basicSalary": 1100,
                        "bonus": 110,
                        "bonusType": "percentage",
                        "salaryWithBonus": 1210
                    }
                ]
            }',
            $client->getResponse()->getContent()
        );
    }
}
