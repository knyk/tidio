<?php

declare(strict_types=1);

namespace App\Tests;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait DoctrineFixturesTrait
{
    public function loadDoctrineFixtures(ContainerInterface $container, FixtureInterface ...$fixtures): void
    {
        $entityManager = $container->get('doctrine.orm.default_entity_manager');

        $loader = new Loader();

        foreach ($fixtures as $fixture) {
            $loader->addFixture($fixture);
        }

        $purger = new ORMPurger($entityManager);
        $executor = new ORMExecutor($entityManager, $purger);

        $executor->execute($loader->getFixtures());
    }

    public function purgeDoctrineDatabase(ContainerInterface $container): void
    {
        $entityManager = $container->get('doctrine.orm.default_entity_manager');

        $purger = new ORMPurger($entityManager);

        $purger->purge();
    }
}
