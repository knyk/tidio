<?php

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->exclude('var')
    ->in(__DIR__);

$config = new \PhpCsFixer\Config();
$config
    ->setRules(
        [
            '@PSR12' => true,
            '@Symfony' => true,
            '@DoctrineAnnotation' => true,
            'array_indentation' => true,
            'no_useless_return' => true,
            'no_useless_else' => true,
            'strict_comparison' => true,
            'strict_param' => true,
            'heredoc_indentation' => true,
            'array_syntax' => ['syntax' => 'short'],
            'declare_strict_types' => true,
            'fully_qualified_strict_types' => true,
            'single_line_throw' => false,
            'concat_space' => [
                'spacing' => 'one',
            ],
            'no_null_property_initialization' => true,
            'method_argument_space' => [
                'on_multiline' => 'ensure_fully_multiline',
            ],
            'constant_case' => [
                'case' => 'lower',
            ],
            'visibility_required' => [
                'elements' => [
                    'property',
                    'method',
                    'const',
                ],
            ],
            'class_attributes_separation' => [
                'elements' => [
                    'method' => 'one',
                ],
            ],
        ]
    )
    ->setRiskyAllowed(true)
    ->setFinder($finder);

return $config;
