<?php

declare(strict_types=1);

namespace App\Application\Handler;

use App\Application\Query\Model\Factory\SalaryReportResultFactory;
use App\Application\Query\Model\SalaryReportResult;
use App\Application\Query\SalaryReport;
use App\Application\Query\Sort\SalaryReportResultSorter;
use App\Domain\Employee\EmployeesRepository;

final class SalaryReportHandler
{
    public function __construct(
        private EmployeesRepository $employeesRepository,
        private SalaryReportResultFactory $salaryReportResultFactory,
        private SalaryReportResultSorter $salaryReportResultSorter
    ) {
    }

    public function __invoke(SalaryReport $salaryReport): SalaryReportResult
    {
        $employees = $this->employeesRepository->list($salaryReport->listCriteria());

        $salaryReportResult = $this->salaryReportResultFactory->create(...$employees);

        if (null !== $salaryReport->salaryReportSort()->field()) {
            $sortedItems = $this->salaryReportResultSorter->sort(
                $salaryReport->salaryReportSort()->field(),
                $salaryReport->salaryReportSort()->isAscending(),
                $salaryReportResult->items
            );

            $salaryReportResult->items = $sortedItems;
        }

        return $salaryReportResult;
    }
}
