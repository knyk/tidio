<?php

declare(strict_types=1);

namespace App\Application\Query\Model;

class SalaryReportResult
{
    /**
     * @var SalaryReportRow[]
     */
    public array $items;
}
