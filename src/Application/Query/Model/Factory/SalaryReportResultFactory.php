<?php

declare(strict_types=1);

namespace App\Application\Query\Model\Factory;

use App\Application\Query\Model\SalaryReportResult;
use App\Application\Query\Model\SalaryReportRow;
use App\Domain\Department\Calculator\BonusCalculator;
use App\Domain\Employee\Employee;

class SalaryReportResultFactory
{
    public function __construct(
        private BonusCalculator $bonusCalculator
    ) {
    }

    public function create(Employee ...$employees): SalaryReportResult
    {
        $salaryReportResult = new SalaryReportResult();

        foreach ($employees as $employee) {
            $bonus = $this->bonusCalculator->calculate($employee);

            $salaryReportRow = new SalaryReportRow();
            $salaryReportRow->firstname = $employee->firstname();
            $salaryReportRow->surname = $employee->surname();
            $salaryReportRow->department = $employee->department()->name();
            $salaryReportRow->basicSalary = $employee->salary();
            $salaryReportRow->bonus = $bonus;
            $salaryReportRow->bonusType = $employee->department()->bonus()->type()->type();
            $salaryReportRow->salaryWithBonus = $employee->salary() + $bonus;

            $salaryReportResult->items[] = $salaryReportRow;
        }

        return $salaryReportResult;
    }
}
