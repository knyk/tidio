<?php

declare(strict_types=1);

namespace App\Application\Query\Model;

class SalaryReportRow
{
    public string $firstname;
    public string $surname;
    public string $department;
    public float $basicSalary;
    public float $bonus;
    public string $bonusType;
    public float $salaryWithBonus;
}
