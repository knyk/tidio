<?php

declare(strict_types=1);

namespace App\Application\Query\Sort;

use App\Application\Query\Model\SalaryReportRow;

class SalaryReportResultSorter
{
    public function sort(string $field, bool $ascending, array $array): array
    {
        usort($array, static fn (SalaryReportRow $a, SalaryReportRow $b) => $a->$field <=> $b->$field);

        if (!$ascending) {
            $array = array_reverse($array);
        }

        return $array;
    }
}
