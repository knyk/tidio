<?php

declare(strict_types=1);

namespace App\Application\Query\Sort;

use App\Application\Query\Model\SalaryReportRow;
use App\Application\Query\Sort\Exception\InvalidSortDirection;
use App\Application\Query\Sort\Exception\InvalidSortField;

class SalaryReportSort
{
    private const SORT_DIRECTION_ASC = 'asc';
    private const SORT_DIRECTION_DESC = 'desc';

    private const AVAILABLE_SORT_DIRECTIONS = [
        self::SORT_DIRECTION_ASC,
        self::SORT_DIRECTION_DESC,
    ];

    public function __construct(
        private ?string $field = null,
        private ?string $direction = null
    ) {
        if ($this->field && !property_exists(SalaryReportRow::class, $this->field)) {
            throw InvalidSortField::withField($this->field);
        }

        if ($this->direction && !in_array($this->direction, self::AVAILABLE_SORT_DIRECTIONS, true)) {
            throw InvalidSortDirection::withDirection($this->direction);
        }
    }

    public function field(): ?string
    {
        return $this->field;
    }

    public function isAscending(): bool
    {
        return self::SORT_DIRECTION_ASC === $this->direction;
    }
}
