<?php

declare(strict_types=1);

namespace App\Application\Query\Sort\Exception;

final class InvalidSortField extends \InvalidArgumentException
{
    public static function withField(string $field): self
    {
        return new self(sprintf('Invalid sort field "%s"', $field));
    }
}
