<?php

declare(strict_types=1);

namespace App\Application\Query\Sort\Exception;

final class InvalidSortDirection extends \InvalidArgumentException
{
    public static function withDirection(string $direction): self
    {
        return new self(sprintf('Invalid sort direction "%s".', $direction));
    }
}
