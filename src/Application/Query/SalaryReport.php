<?php

declare(strict_types=1);

namespace App\Application\Query;

use App\Application\Query\Sort\SalaryReportSort;
use App\Domain\Employee\Criteria\ListCriteria;

final class SalaryReport
{
    public function __construct(
        private ListCriteria $listCriteria,
        private SalaryReportSort $salaryReportSort
    ) {
    }

    public function listCriteria(): ListCriteria
    {
        return $this->listCriteria;
    }

    public function salaryReportSort(): SalaryReportSort
    {
        return $this->salaryReportSort;
    }
}
