<?php

declare(strict_types=1);

namespace App\Domain\Department;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'departments')]
class Department
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    public function __construct(
        #[ORM\Column(name: 'name', type: 'string', nullable: false)]
        private string $name,
        #[ORM\OneToOne(targetEntity: Bonus::class, cascade: ['persist'])]
        private Bonus $bonus,
    ) {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function bonus(): Bonus
    {
        return $this->bonus;
    }
}
