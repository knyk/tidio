<?php

declare(strict_types=1);

namespace App\Domain\Department\Calculator;

use App\Domain\Department\Calculator\Exception\NotSatisfiedCalculationStrategy;
use App\Domain\Department\Calculator\Strategy\BonusCalculationStrategy;
use App\Domain\Employee\Employee;

class BonusCalculator
{
    private array $bonusCalculationStrategies;

    public function __construct(BonusCalculationStrategy ...$bonusCalculationStrategies)
    {
        $this->bonusCalculationStrategies = $bonusCalculationStrategies;
    }

    public function calculate(Employee $employee): float
    {
        foreach ($this->bonusCalculationStrategies as $bonusCalculationStrategy) {
            if ($bonusCalculationStrategy->isSatisfiedBy($employee->department()->bonus()->type())) {
                return $bonusCalculationStrategy->calculate($employee);
            }
        }

        throw NotSatisfiedCalculationStrategy::withBonusType($employee->department()->bonus()->type()->type());
    }
}
