<?php

declare(strict_types=1);

namespace App\Domain\Department\Calculator\Strategy;

use App\Domain\Department\BonusType;
use App\Domain\Employee\Employee;
use App\Domain\Shared\Provider\DateTimeProvider;

final class AnnualBonusCalculationStrategy implements BonusCalculationStrategy
{
    private const BONUS_LIMIT_IN_YEARS = 10;

    public function __construct(
        private DateTimeProvider $dateTimeProvider
    ) {
    }

    public function isSatisfiedBy(BonusType $bonusType): bool
    {
        return $bonusType->equalTo(BonusType::annual());
    }

    public function calculate(Employee $employee): float
    {
        $yearsToBeConsidered = $this->calculateYearsOfBonus($employee->employedSince());

        return $yearsToBeConsidered * $employee->department()->bonus()->value();
    }

    private function calculateYearsOfBonus(\DateTimeInterface $employedSince): int
    {
        $dateDiff = $employedSince->diff($this->dateTimeProvider->now());
        $workExperienceInYears = $dateDiff->y;
        if ($workExperienceInYears < self::BONUS_LIMIT_IN_YEARS) {
            return $workExperienceInYears;
        }

        return self::BONUS_LIMIT_IN_YEARS;
    }
}
