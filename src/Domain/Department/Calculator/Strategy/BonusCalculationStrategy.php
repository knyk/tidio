<?php

declare(strict_types=1);

namespace App\Domain\Department\Calculator\Strategy;

use App\Domain\Employee\Employee;

interface BonusCalculationStrategy extends BonusCalculationSpecification
{
    public function calculate(Employee $employee): float;
}
