<?php

declare(strict_types=1);

namespace App\Domain\Department\Calculator\Strategy;

use App\Domain\Department\BonusType;

interface BonusCalculationSpecification
{
    public function isSatisfiedBy(BonusType $bonusType): bool;
}
