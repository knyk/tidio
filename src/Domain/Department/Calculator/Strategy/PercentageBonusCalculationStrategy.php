<?php

declare(strict_types=1);

namespace App\Domain\Department\Calculator\Strategy;

use App\Domain\Department\BonusType;
use App\Domain\Employee\Employee;

final class PercentageBonusCalculationStrategy implements BonusCalculationStrategy
{
    public function isSatisfiedBy(BonusType $bonusType): bool
    {
        return $bonusType->equalTo(BonusType::percentage());
    }

    public function calculate(Employee $employee): float
    {
        return $employee->salary() * ($employee->department()->bonus()->value() / 100);
    }
}
