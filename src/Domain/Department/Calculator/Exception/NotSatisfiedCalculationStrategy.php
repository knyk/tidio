<?php

declare(strict_types=1);

namespace App\Domain\Department\Calculator\Exception;

final class NotSatisfiedCalculationStrategy extends \DomainException
{
    public static function withBonusType(string $bonusType): self
    {
        return new self(sprintf('Couln\'t find strategy satisfied by "%s" bonus type.', $bonusType));
    }
}
