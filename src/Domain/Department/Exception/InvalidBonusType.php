<?php

declare(strict_types=1);

namespace App\Domain\Department\Exception;

final class InvalidBonusType extends \DomainException
{
    public static function withType(string $type): self
    {
        return new self(sprintf('Invalid bonus type provided: %s.', $type));
    }
}
