<?php

declare(strict_types=1);

namespace App\Domain\Department;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'bonuses')]
class Bonus
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    public function __construct(
        #[ORM\Embedded(class: BonusType::class, columnPrefix: false)]
        private BonusType $type,
        #[ORM\Column(name: 'value', type: 'float', nullable: false)]
        private float $value,
    ) {
    }

    public function type(): BonusType
    {
        return $this->type;
    }

    public function value(): float
    {
        return $this->value;
    }
}
