<?php

declare(strict_types=1);

namespace App\Domain\Department;

use App\Domain\Department\Exception\InvalidBonusType;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class BonusType
{
    private const TYPE_ANNUAL = 'annual';
    private const TYPE_PERCENTAGE = 'percentage';

    private const SUPPORTED_TYPES = [
        self::TYPE_ANNUAL,
        self::TYPE_PERCENTAGE,
    ];

    public function __construct(
        #[ORM\Column(name: 'type', type: 'string', nullable: false)]
        private string $type
    ) {
        if (!in_array($this->type, self::SUPPORTED_TYPES, true)) {
            throw InvalidBonusType::withType($this->type);
        }
    }

    public static function annual(): self
    {
        return new self(self::TYPE_ANNUAL);
    }

    public static function percentage(): self
    {
        return new self(self::TYPE_PERCENTAGE);
    }

    public function type(): string
    {
        return $this->type;
    }

    public function equalTo(self $bonusType): bool
    {
        return $this->type === $bonusType->type;
    }
}
