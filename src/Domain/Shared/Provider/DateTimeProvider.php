<?php

declare(strict_types=1);

namespace App\Domain\Shared\Provider;

interface DateTimeProvider
{
    public function now(): \DateTimeInterface;
}
