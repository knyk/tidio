<?php

declare(strict_types=1);

namespace App\Domain\Employee;

use App\Domain\Department\Department;
use App\Infrastructure\Repository\Doctrine\EmployeesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmployeesRepository::class)]
#[ORM\Table(name: 'employees')]
class Employee
{
    #[ORM\Id]
    #[ORM\Column(name: 'id', type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    public function __construct(
        #[ORM\Column(name: 'firstname', type: 'string', nullable: false)]
        private string $firstname,
        #[ORM\Column(name: 'surname', type: 'string', nullable: false)]
        private string $surname,
        #[ORM\Column(name: 'salary', type: 'float', nullable: false)]
        private float $salary,
        #[ORM\Column(name: 'employes_since', type: 'date_immutable', nullable: false)]
        private \DateTimeImmutable $employedSince,
        #[ORM\ManyToOne(targetEntity: Department::class)]
        private Department $department,
    ) {
    }

    public function firstname(): string
    {
        return $this->firstname;
    }

    public function surname(): string
    {
        return $this->surname;
    }

    public function salary(): float
    {
        return $this->salary;
    }

    public function employedSince(): \DateTimeImmutable
    {
        return $this->employedSince;
    }

    public function department(): Department
    {
        return $this->department;
    }
}
