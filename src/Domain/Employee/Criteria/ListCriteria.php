<?php

declare(strict_types=1);

namespace App\Domain\Employee\Criteria;

class ListCriteria
{
    public function __construct(
        private ?string $firstname,
        private ?string $surname,
        private ?string $department,
    ) {
    }

    public function firstname(): ?string
    {
        return $this->firstname;
    }

    public function surname(): ?string
    {
        return $this->surname;
    }

    public function department(): ?string
    {
        return $this->department;
    }
}
