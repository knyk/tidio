<?php

declare(strict_types=1);

namespace App\Domain\Employee;

use App\Domain\Employee\Criteria\ListCriteria;

interface EmployeesRepository
{
    /**
     * @return Employee[]
     */
    public function list(ListCriteria $criteria): array;
}
