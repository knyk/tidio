<?php

declare(strict_types=1);

namespace App\Infrastructure\Fixtures\Doctrine;

use App\Domain\Department\Bonus;
use App\Domain\Department\BonusType;
use App\Domain\Department\Department;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class DepartmentsFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $departmentHr = new Department('HR', new Bonus(BonusType::annual(), 100));
        $this->setReference('department-hr', $departmentHr);
        $manager->persist($departmentHr);

        $departmentBok = new Department('BOK', new Bonus(BonusType::percentage(), 10));
        $this->setReference('department-bok', $departmentBok);
        $manager->persist($departmentBok);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 0;
    }
}
