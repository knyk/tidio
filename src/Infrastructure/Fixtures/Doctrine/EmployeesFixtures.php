<?php

declare(strict_types=1);

namespace App\Infrastructure\Fixtures\Doctrine;

use App\Domain\Department\Department;
use App\Domain\Employee\Employee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

final class EmployeesFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Department $departmentHr */
        $departmentHr = $this->getReference('department-hr');

        $adamKowalski = new Employee(
            'Adam',
            'Kowalski',
            1000,
            new \DateTimeImmutable('2006-07-19'),
            $departmentHr
        );
        $manager->persist($adamKowalski);

        /** @var Department $departmentBok */
        $departmentBok = $this->getReference('department-bok');

        $aniaNowak = new Employee(
            'Ania',
            'Nowak',
            1100,
            new \DateTimeImmutable('2016-07-19'),
            $departmentBok
        );
        $manager->persist($aniaNowak);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 1;
    }
}
