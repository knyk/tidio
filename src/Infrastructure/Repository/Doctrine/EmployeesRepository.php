<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Doctrine;

use App\Domain\Employee\Criteria\ListCriteria;
use App\Domain\Employee\Employee;
use App\Domain\Employee\EmployeesRepository as EmployeesRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

final class EmployeesRepository extends ServiceEntityRepository implements EmployeesRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    public function list(ListCriteria $criteria): array
    {
        $query = $this->createQueryBuilder('e');

        $query->leftJoin('e.department', 'd', Join::WITH)
            ->leftJoin('d.bonus', 'b', Join::WITH);

        if ($criteria->firstname()) {
            $query->andWhere('e.firstname = :firstname')
                ->setParameter('firstname', $criteria->firstname());
        }

        if ($criteria->surname()) {
            $query->andWhere('e.surname = :surname')
                ->setParameter('surname', $criteria->surname());
        }

        if ($criteria->department()) {
            $query->andWhere('d.name = :department')
                ->setParameter('department', $criteria->department());
        }

        return $query->getQuery()->getResult();
    }
}
