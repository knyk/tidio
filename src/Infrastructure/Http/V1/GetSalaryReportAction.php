<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\V1;

use App\Application\Query\SalaryReport;
use App\Application\Query\Sort\SalaryReportSort;
use App\Domain\Employee\Criteria\ListCriteria;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/v1/salary-reports')]
final class GetSalaryReportAction
{
    public function __invoke(
        ListCriteria $listCriteria,
        SalaryReportSort $salaryReportSort,
        MessageBusInterface $messageBus
    ): JsonResponse {
        $envelope = $messageBus->dispatch(new SalaryReport($listCriteria, $salaryReportSort));
        /** @var HandledStamp $handledStamp */
        $handledStamp = $envelope->last(HandledStamp::class);

        $salaryReport = $handledStamp->getResult();

        return new JsonResponse($salaryReport);
    }
}
