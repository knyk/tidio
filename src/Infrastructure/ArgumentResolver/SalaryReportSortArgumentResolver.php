<?php

declare(strict_types=1);

namespace App\Infrastructure\ArgumentResolver;

use App\Application\Query\Sort\SalaryReportSort;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class SalaryReportSortArgumentResolver implements ArgumentValueResolverInterface
{
    private const SORT_FIELD_QUERY_PARAM = 'sortField';
    private const SORT_DIRECTION_QUERY_PARAM = 'sortDirection';

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return SalaryReportSort::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        yield new SalaryReportSort(
            /* @phpstan-ignore-next-line */
            $request->query->get(self::SORT_FIELD_QUERY_PARAM),
            /* @phpstan-ignore-next-line */
            $request->query->get(self::SORT_DIRECTION_QUERY_PARAM)
        );
    }
}
