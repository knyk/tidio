<?php

declare(strict_types=1);

namespace App\Infrastructure\ArgumentResolver;

use App\Domain\Employee\Criteria\ListCriteria;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class EmployeesListCriteriaArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ListCriteria::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        yield new ListCriteria(
            $request->query->getAlnum('firstname'),
            $request->query->getAlnum('surname'),
            $request->query->getAlnum('department'),
        );
    }
}
