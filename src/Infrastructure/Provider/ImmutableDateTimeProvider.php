<?php

declare(strict_types=1);

namespace App\Infrastructure\Provider;

use App\Domain\Shared\Provider\DateTimeProvider;

final class ImmutableDateTimeProvider implements DateTimeProvider
{
    public function now(): \DateTimeInterface
    {
        return new \DateTimeImmutable();
    }
}
