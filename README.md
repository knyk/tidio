# Tidio - zadanie testowe

## Założenia
* istnieją dwa sposoby liczenia dodatku: procentowy i w zależności od stażu
* istnieje interface pozwalający pobrać raport
* raport można sortować po dowolnej kolumnie
* raport można filtrować po imieniu, nazwisku i dziale
* można dodawać nowe działy (brak interfejsu)
* można dodawać nowych pracowników (brak interfejsu)
* rozwiązanie oparte o Symfony lub Laravel
* aplikacja się uruchamia i zwraca poprawne wyniki

## Architektura rozwiązania

Przyjęte zostało rozwiązanie oparte o architekturę warstwową, framework agnostic.
Jako warstwę infrastruktury wykorzystany został framework Symfony, jako query bus Symfony Messenger.

Do uruchamiania aplikacji zostało przygotowane środowisko oparte o docker-compose składające się z kontenera PHP 8, MySql 8 oraz nginx 1.21. Dodatkowo przygotowany dostał plik Makefile z komendami ułatwiającymi uruchomienie środowiska.

Środowisko testowane było w środowisku linux na dystrybucji Kubuntu 20.04, docker 20.10.07 oraz docker-compose 1.28.2.

Kod projektu został pokryty testami jednostkowymi napisanymi w `phpspec/phpspec` oraz testami funkcjonalnymi napisanymi w `phpunit/phpunit`.

## Metodologia

W rozwiązaniu przyjęte zostało założenie, że dodatek powinien być liczony w kodzie a nie w bazie danych aby umożliwić swobodne modyfikowanie istniejących oraz dodawanie nowych sposobów liczenia dodatków.

W tym celu powstał w warstwie domeny interface `App\Domain\Department\Calculator\Strategy\BonusCalculationStrategy`, który należy zaimplementować w przypadku implementacji nowego sposobu liczenia dodatku.

W związku z dynamicznym liczeniem dodatków, pojawił się problem z implementacją sortowania po dowolnej kolumnie – niemożliwe stało się sortowanie wyników na poziomie bazy danych.

Rozwiązaniem alternatywnym mogłoby być przeniesienia sortowania kolumn, które nie są generowane dynamicznie do bazy danych, jednak rozwiązanie takie niepotrzebnie komplikuje kod.

Innym rozwiązaniem tego problemu mogłoby być dodanie kolejnego modelu przy pracowniku, w którym przetrzymywane by były zmiany wysokości dodatku w czasie lub nowej danej przy pracowniku, gdzie była by aktualizowana aktualna wysokość dodatku. W takim przypadku należałoby dodać zadanie wykonywane zgodnie z przyjętym harmonogramem, które by aktualizowało np., raz dziennie, aktualne stawki dodatków dla pracowników.

Oba alternatywne rozwiązania wprowadzają niepotrzebną komplikacje w kodzie i logice działania aplikacji. Z tego powodu zostały one odrzucone.

W związku z tym, że dodatki są liczone w locie oraz sortowanie odbywa się na poziomie PHP rozwiązanie nie jest optymalne.

Proponowanym rozwiązaniem umożliwiającym optymalizację aktualnego rozwiązania byłoby kolejkowanie generowania raportów i tzw. zamawianie raportów poprzez API. Aktualnie dostępny endpoint zwracał by ID przydzielone zamówionemu raportowi oraz dodatkowo stworzony zostałby kolejny endpoint, który zwracał by gotowy raport w momencie kiedy byłby on już gotowy. Takie rozwiązanie jest stosowane na szeroką skalę w wielu systemach.

## Uruchomienie projektu

Aby uruchomić projekt należy w konsoli wpisać komendę `make run` w głównym katalogu projektu. Istnieje prawdopodobieństwo, że pojawi się błąd związany z niedostępnością bazy danych – tutaj przydałby się w przyszłości skrypt czekający na dostępność mysql. W takim przypadku wystarczy powtórzyć poprzednią komendę.

Projekt wykorzystuje [docker-hostamanager](https://github.com/iamluc/docker-hostmanager) co oznacza że jeśli jest on dostępny w systemie uruchomieniowym, dostępny będzie host nginxa pod adresem [http://tidio.loc](http://tidio.loc). Jeśli docker-hostmanager nie jest dostępny, należy łączyć się do [http://localhost:8080](http://localhost:8080).

Po uruchomieniu dostępny powinien być endpoint `GET /api/v1/salary-reports` zwracający raport zgodny z wymaganiami zadania w formacie JSON.

Filtrowanie możliwe jest po podaniu parametru url, np `?firstname=adam` gdzie `firstname` jest nazwą pola po którym chcemy filtrować.

Sortowanie odbywa się poprzez parametry url `?sortField=firstname&sortDirection=asc` gdzie `sortField` jest nazwą pola po której chcemy sortować a `sortDirection` przyjmuje wartości `asc` lub `desc` w zależności od kolejności, którą chcemy otrzymać.

Przykładowy response z endpointu:

```json
{
    "items": [
        {
            "firstname": "Adam",
            "surname": "Kowalski",
            "department": "HR",
            "basicSalary": 1000,
            "bonus": 1000,
            "bonusType": "annual",
            "salaryWithBonus": 2000
        },
        {
            "firstname": "Jan",
            "surname": "Nowak",
            "department": "HR",
            "basicSalary": 1500,
            "bonus": 100,
            "bonusType": "annual",
            "salaryWithBonus": 1600
        },
        {
            "firstname": "Ania",
            "surname": "Nowak",
            "department": "BOK",
            "basicSalary": 1100,
            "bonus": 110,
            "bonusType": "percentage",
            "salaryWithBonus": 1210
        }
    ]
}
```

## Testy

Testy znajdują się w:
* /spec – testy jednostkowe napisane w `phpspec`
* /tests – testy funkcjonalne w `phpunit`

Dodatkowo do projektu zostały dołączone narzędzia do statycznej analizy kodu:
* phpstan
* php-cs-fixer
* php-codesniffer
* magic number detector

Nie został dołączony `php mess detector` z powodu braku kompatybilności z promowaniem zmiennych konstruktorem.

Aby odpalić cały zestaw testów i statycznej analizy kodu wystarczy odpalić w konsoli `make tests` w katalogu głównym projektu.

## CI

Projekt posiada integrację z GitLab CI. Przy okazji każdego commita odpalany jest cały zestaw testów i analizy.

CI zostało oparte o wcześniej zbudowany obraz z Dockerfile dostępnego w projekcie i wypchnięcie go do rejestru obrazów w repozytorium projektu. Taki zabieg był wymagany z dowodu braku możliwości zbudowania obrazu w trakcie pipeline’a z uwagi na ograniczenia darmowego planu runnerów gitlabowych.

W normalnych warunkach w trakcie pipeline’a powinien zostać zbudowane obrazy na podstawie Dockerfile projektu, na tych obrazach odpalone testy a następnie w niezmienionej formie powinny zostać wypchnięte do rejestru w celu ewentualnego deploymentu.
