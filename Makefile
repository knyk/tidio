.PHONY: help
help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

sh:
	docker-compose exec php sh

up:
	docker-compose up -d

build:
	docker-compose build

pull:
	docker-compose pull

down:
	-docker-compose down -v

run-migrations:
	-docker-compose exec php php bin/console doctrine:migration:migrate --no-interaction

load-fixtures:
	-docker-compose exec php php bin/console doctrine:fixtures:load --no-interaction

cache-clear:
	-docker-compose exec php bin/console c:c
	-docker-compose exec php bin/console c:c --env=test

composer-install:
	docker-compose exec php composer install

sleep:
	sleep 10

run: up composer-install sleep cache-clear run-migrations load-fixtures

tests-steps:
	docker-compose exec php composer qa

.PHONY: tests
tests: run composer-install tests-steps

tests-fast: up cache-clear tests-steps

fix-code:
	docker-compose run php composer cs-fix

analyse:
	docker-compose run php composer sa
