<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210719154355 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE employees ADD employes_since DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', DROP work_experience_if_years');
    }

    public function down(Schema $schema): void
    {
    }
}
